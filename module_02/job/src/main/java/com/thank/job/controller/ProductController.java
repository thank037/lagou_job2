package com.thank.job.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * description: none
 *
 * @author thank
 * 2021/5/19 18:08
 */
@RestController
@RequestMapping("product")
public class ProductController {

    @Autowired
    private Environment environment;

    private Integer port = 8081;

    @GetMapping("findAll")
    public Object findAll() {
        System.out.println(environment.getProperty("local.server.port"));
        Map<String, Object> productMap = new HashMap<>(2);
        productMap.put("port", port);
        productMap.put("info", "商品信息");
        return productMap;
    }
}
