package com.thank.job.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.UUID;

/**
 * description: none
 *
 * @author thank
 * 2021/5/19 17:45
 */
@RestController
@RequestMapping("user")
public class LoginController {

    @GetMapping("login")
    public String login(@RequestParam String username, @RequestParam String password, HttpSession session) {

        if ("admin".equals(username) && "123".equals(password)) {
            session.setAttribute("sessionId", username + "-" + UUID.randomUUID());
        } else {
            return "Invalid username or password";
        }

        return "login success!";
    }
}
