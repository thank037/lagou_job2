package com.thank.job.interceptor;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * description: 请求登录拦截验证
 *
 * @author thank
 * 2021/5/19 18:16
 */
public class LoginInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession session = request.getSession();
        Object sessionId = session.getAttribute("sessionId");
        if (sessionId == null) {
            response.setContentType("text/html; charset=utf-8");
            response.getWriter().write("<h1>未登录, 去登录吧</h1>");
            System.out.println("未登录");
            return false;
        } else {
            System.out.println("已登录");
            return true;
        }
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
    }
}
