import bean.Request;
import bean.Response;
import classloader.MinicatClassLoader;
import container.Context;
import container.Host;
import container.Wrapper;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import servlet.HttpServlet;
import thread.RequestProcessor;
import thread.RequestProcessorV4;
import utils.HttpProtocolUtil;
import utils.StaticResourceUtil;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * description: Mini tomcat主类
 *
 * @author thank
 * 2021/5/15 23:44
 */
public class Bootstrap {

    /** 定义Socket监听的端口号 */
    private int port = 8080;

    /** 从web.xml中的servlet配置中读取并初始化servlet */
    Map<String, HttpServlet> urlToServletMap = new HashMap<>();

    private Host host;

    private final ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(
            10, 50, 100L, TimeUnit.SECONDS,
            new ArrayBlockingQueue<>(50),
            Executors.defaultThreadFactory(),
            new ThreadPoolExecutor.AbortPolicy()
    );

    public static void main(String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        try {
            bootstrap.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void start() throws Exception {

        loadServlet();

        loadServer();

        ServerSocket serverSocket = new ServerSocket(port);

        printInfo();

        while (true) {
            Socket socket = serverSocket.accept();
            // processV1(socket);
            // processV2(socket);
            // processV3_0(socket);
            // processV3_1(socket);
            // processV3_2(socket);
            processV4(socket);
        }

    }

    void printInfo() {

        System.out.println("----------------------------------------------------------------------------");
        System.out.println("Mini-tomcat startup on port: " + port);
        System.out.println("Host: " + host.getName());
        System.out.println("AppBase: " + host.getAppBase());

        for (Context context : host.getContexts()) {
            System.out.println("\t App Context: " + context.getName());
            for (String url : context.getWrapperMap().keySet()) {
                System.out.println("\t\t Wrapper: " + url);
            }
        }
        System.out.println("----------------------------------------------------------------------------");
    }


    /**
     *  加载解析web.xml，初始化Servlet
     */
    public void loadServlet() {

        InputStream inputStream = StaticResourceUtil.class.getClassLoader().getResourceAsStream("web.xml");
        SAXReader saxReader = new SAXReader();
        try {
            Document document = saxReader.read(inputStream);
            Element rootElement = document.getRootElement();
            List<Element> servletElement = rootElement.selectNodes("//servlet");
            for (Element element : servletElement) {

                //  <servlet-name>hello</servlet-name>
                Element servletNameElement = (Element) element.selectSingleNode("servlet-name");
                String servletName = servletNameElement.getStringValue();

                // <servlet-class>servlet.HelloServlet</servlet-class>
                Element servletClassElement = (Element) element.selectSingleNode("servlet-class");
                String servletClass = servletClassElement.getStringValue();

                // 根据servlet-name的值找到url-pattern
                Element servletMappingElement = (Element) rootElement.selectSingleNode("/web-app/servlet-mapping[servlet-name='" + servletName + "']");
                // <url-pattern>/hello</url-pattern>
                String urlPattern = servletMappingElement.selectSingleNode("url-pattern").getStringValue();

                urlToServletMap.put(urlPattern, (HttpServlet) Class.forName(servletClass).newInstance());

            }
        } catch (DocumentException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }


    /**
     * 封装Mapper组件体系
     * - Mapper类—>Host->Context->Wrapper->Servlet
     */
    private void loadServer() {
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("server.xml");
        SAXReader saxReader = new SAXReader();
        try {
            Document document = saxReader.read(inputStream);
            Element rootElement = document.getRootElement();
            Element connectorElement = (Element) rootElement.selectSingleNode("Service/Connector");

            String portStr = connectorElement.attributeValue("port");
            if (portStr != null && !"".equals(portStr.trim())) {
                this.port = Integer.parseInt(portStr);
            }

            Element hostElement = (Element) rootElement.selectSingleNode("Service/Engine/Host");
            this.host = new Host();
            this.host.setAppBase(hostElement.attributeValue("appBase"));
            this.host.setName(hostElement.attributeValue("name"));

            loadContext(host);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void loadContext(Host host) throws MalformedURLException {
        String appBase = host.getAppBase();
        File appBaseFile = new File(appBase);
        if (!appBaseFile.exists()) {
            return;
        }
        File[] files = appBaseFile.listFiles();
        if (files == null) {
            return;
        }
        for (File webAppDir : files) {
            if (webAppDir.isDirectory()) {
                Context context = new Context();
                context.setName(webAppDir.getName());
                context.setClassLoader(new MinicatClassLoader(new URL[]{webAppDir.toURI().toURL()}));
                File webXmlFile = new File(webAppDir, "/web.xml");
                if (webXmlFile.exists()) {
                    loadWrapper(context, webXmlFile);
                }
                host.getContexts().add(context);
            }
        }

    }

    private void loadWrapper(Context context, File webXmlFile) {
        try {
            SAXReader saxReader = new SAXReader();
            Document document = saxReader.read(webXmlFile);
            Element rootElement = document.getRootElement();
            List<Element> servletElement = rootElement.selectNodes("//servlet");
            for (Element element : servletElement) {

                //  <servlet-name>hello</servlet-name>
                Element servletNameElement = (Element) element.selectSingleNode("servlet-name");
                String servletName = servletNameElement.getStringValue();

                // <servlet-class>servlet.HelloServlet</servlet-class>
                Element servletClassElement = (Element) element.selectSingleNode("servlet-class");
                String servletClass = servletClassElement.getStringValue();

                // 根据servlet-name的值找到url-pattern
                Element servletMappingElement = (Element) rootElement.selectSingleNode("/web-app/servlet-mapping[servlet-name='" + servletName + "']");
                // <url-pattern>/hello</url-pattern>
                String urlPattern = servletMappingElement.selectSingleNode("url-pattern").getStringValue();

                Wrapper wrapper = new Wrapper();
                wrapper.setServletName(servletName);
                wrapper.setServletClass(servletClass);
                wrapper.setUrlPattern(urlPattern);

                ClassLoader classLoader = context.getClassLoader();
                wrapper.setServlet((HttpServlet) classLoader.loadClass(servletClass).newInstance());
//                wrapper.setServlet((HttpServlet) Class.forName(servletClass).newInstance());

                context.getWrapperMap().put(urlPattern, wrapper);
            }
        } catch (DocumentException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }

    }


    /**
     * v1.0: 需求：浏览器请求http://localhost:8080, 返回一个固定的字符串到页面
     */
    void processV1(Socket socket) throws Exception {
        // 有了socket，接收到请求，获取输出流
        OutputStream outputStream = socket.getOutputStream();

        String content = "Hello! Mini-tomcat";
        String responseText = HttpProtocolUtil.getHttpHeader200(content.length()) + content;
        outputStream.write(responseText.getBytes());
        socket.close();
    }

    /**
     * v2.0: 需求：封装Request和Response对象，能返回html静态资源文件: `index.html`
     */
    void processV2(Socket socket) throws Exception {
        Request request = new Request(socket.getInputStream());
        Response response = new Response(socket.getOutputStream());
        response.outputHtml(null, request.getUrl());
        socket.close();
    }

    /**
     * v3.0: 需求：可以请求动态资源（Servlet）
     */
    void processV3_0(Socket socket) throws Exception {
        Request request = new Request(socket.getInputStream());
        Response response = new Response(socket.getOutputStream());

        if (urlToServletMap.containsKey(request.getUrl())) {
            // 动态servlet处理
            HttpServlet httpServlet = urlToServletMap.get(request.getUrl());
            httpServlet.service(request, response);
        } else {
            // 静态资源处理
            response.outputHtml(null, request.getUrl());
        }

        socket.close();
    }

    /** v3.1 (v3.0 + 使用多线程) */
    void processV3_1(Socket socket) throws Exception {
        RequestProcessor processor = new RequestProcessor(socket, urlToServletMap);
        processor.start();
    }

    /** v3.2 (v3.1 + 使用线程池) */
    void processV3_2(Socket socket) throws Exception {
        RequestProcessor processor = new RequestProcessor(socket, urlToServletMap);
        threadPoolExecutor.execute(processor);
    }

    /**
     * 作业
     */
    private void processV4(Socket socket) throws Exception {
        RequestProcessorV4 processor = new RequestProcessorV4(socket, host);
        threadPoolExecutor.execute(processor);
    }

}
