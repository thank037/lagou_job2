package utils;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import servlet.HttpServlet;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * description: none
 *
 * @author thank
 * 2021/5/16 12:47
 */
public class StaticResourceUtil {

    /**
     * 获取该path的绝对路径
     */
    public static String getAbsolutePath(String path) {
        return StaticResourceUtil.class.getResource("/").getPath() + path;
    }

    /**
     * 读取静态资源文件输入流，通过输出流输出
     */
    public static void outputStaticResource(InputStream inputStream, OutputStream outputStream) throws IOException {

        int count = 0;
        while(count == 0) {
            count = inputStream.available();
        }

        int resourceSize = count;
        // 输出http请求头,然后再输出具体内容
        outputStream.write(HttpProtocolUtil.getHttpHeader200(resourceSize).getBytes());

        // 读取内容输出: written: 已经读取的内容长度, byteSize: 计划每次缓冲的长度
        long written = 0 ;
        int byteSize = 1024;
        byte[] bytes = new byte[byteSize];

        while(written < resourceSize) {
            // 说明剩余未读取大小不足一个1024长度，那就按真实长度处理
            if(written + byteSize > resourceSize) {
                // 剩余的文件内容长度
                byteSize = (int) (resourceSize - written);
                bytes = new byte[byteSize];
            }

            inputStream.read(bytes);
            outputStream.write(bytes);

            written += byteSize;
        }

        outputStream.flush();
    }



}
