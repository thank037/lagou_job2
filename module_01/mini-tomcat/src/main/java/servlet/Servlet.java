package servlet;

import bean.Request;
import bean.Response;

/**
 * description: none
 *
 * @author thank
 * 2021/5/16 13:07
 */
public interface Servlet {

    void init() throws Exception;

    void destroy() throws Exception;

    void service(Request request, Response response) throws Exception;
}
