package servlet;

import bean.Request;
import bean.Response;
import utils.HttpProtocolUtil;

/**
 * description: none
 *
 * @author thank
 * 2021/5/16 13:08
 */
public abstract class HttpServlet implements Servlet {

    @Override
    public void service(Request request, Response response) throws Exception {
        if ("GET".equalsIgnoreCase(request.getMethod())) {
            doGet(request, response);
        } else if ("POST".equalsIgnoreCase(request.getMethod())) {
            doPost(request, response);
        } else {
            response.outputStr(HttpProtocolUtil.getHttp405());
        }
    }

    public abstract void doGet(Request request, Response response);
    public abstract void doPost(Request request, Response response);
}
