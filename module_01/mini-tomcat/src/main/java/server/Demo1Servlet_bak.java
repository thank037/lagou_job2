package server;

import bean.Request;
import bean.Response;
import servlet.HttpServlet;
import utils.HttpProtocolUtil;

import java.io.IOException;

/**
 * description: none
 *
 * @author thank
 * 2021/5/16 13:14
 */
public class Demo1Servlet_bak extends HttpServlet {

    @Override
    public void doGet(Request request, Response response) {
        String content = "<h2> Demo1应用的 Demo1Servlet ==> doGet  </h2>";

//        try {
//            // FIXME: 测试阻塞
//            Thread.sleep(100000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

        try {
            response.outputStr(HttpProtocolUtil.getHttpHeader200(content.length()) + content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void doPost(Request request, Response response) {
        String content = "<h2>  HelloServlet ==> doPost  </h2>";
        try {
            response.outputStr(HttpProtocolUtil.getHttpHeader200(content.length()) + content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void init() throws Exception {

    }

    @Override
    public void destroy() throws Exception {

    }
}
