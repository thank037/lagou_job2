import com.google.common.base.CharMatcher;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import container.Context;
import container.Wrapper;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import servlet.HttpServlet;
import utils.StaticResourceUtil;

import java.io.File;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

/**
 * description: none
 *
 * @author thank
 * 2021/5/16 0:28
 */
public class TestMain {

    public static void main(String[] args) throws Exception {

        File classPath = new File("F:\\0_lagou\\code\\stage_02\\module_01\\webapps\\demo1");
        System.out.println(classPath);
        URL[] urls = new URL[1];
        try {
            urls[0] = classPath.toURI().toURL();
            System.out.println(Arrays.toString(urls));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public static void loadWrapper(Context context, File webXmlFile) {
        try {
            SAXReader saxReader = new SAXReader();
            Document document = saxReader.read(webXmlFile);
            Element rootElement = document.getRootElement();
            List<Element> servletElement = rootElement.selectNodes("//servlet");
            for (Element element : servletElement) {

                //  <servlet-name>hello</servlet-name>
                Element servletNameElement = (Element) element.selectSingleNode("servlet-name");
                String servletName = servletNameElement.getStringValue();

                // <servlet-class>servlet.HelloServlet</servlet-class>
                Element servletClassElement = (Element) element.selectSingleNode("servlet-class");
                String servletClass = servletClassElement.getStringValue();

                // 根据servlet-name的值找到url-pattern
                Element servletMappingElement = (Element) rootElement.selectSingleNode("/web-app/servlet-mapping[servlet-name='" + servletName + "']");
                // <url-pattern>/hello</url-pattern>
                String urlPattern = servletMappingElement.selectSingleNode("url-pattern").getStringValue();

                Wrapper wrapper = new Wrapper();
                wrapper.setServletName(servletName);
                wrapper.setServletClass(servletClass);
                wrapper.setUrlPattern(urlPattern);
                wrapper.setServlet((HttpServlet) Class.forName(servletClass).newInstance());

                // TODO: context add wrapper
                context.getWrapperMap().put(urlPattern, wrapper);
            }
        } catch (DocumentException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }

    }
}
