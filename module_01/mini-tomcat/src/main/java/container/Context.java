package container;

import java.util.HashMap;
import java.util.Map;

/**
 * description: none
 *
 * @author thank
 * 2021/5/17 15:51
 */
public class Context {

    private String name;

    private Map<String, Wrapper> wrapperMap = new HashMap<>();

    private ClassLoader classLoader;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, Wrapper> getWrapperMap() {
        return wrapperMap;
    }

    public void setWrapperMap(Map<String, Wrapper> wrapperMap) {
        this.wrapperMap = wrapperMap;
    }

    public ClassLoader getClassLoader() {
        return classLoader;
    }

    public void setClassLoader(ClassLoader classLoader) {
        this.classLoader = classLoader;
    }
}
