package container;

import java.util.ArrayList;
import java.util.List;

/**
 * description: none
 *
 * @author thank
 * 2021/5/17 12:38
 */
public class Host {

    private String name;

    private String appBase;

    private List<Context> contexts = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAppBase() {
        return appBase;
    }

    public void setAppBase(String appBase) {
        this.appBase = appBase;
    }

    public List<Context> getContexts() {
        return contexts;
    }

    public void setContexts(List<Context> contexts) {
        this.contexts = contexts;
    }
}
