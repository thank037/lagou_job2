package container;

import servlet.HttpServlet;

/**
 * description: none
 *
 * @author thank
 * 2021/5/17 15:37
 */
public class Wrapper {

    private HttpServlet servlet;

    private String servletName;

    private String servletClass;

    private String urlPattern;



    public HttpServlet getServlet() {
        return servlet;
    }

    public void setServlet(HttpServlet servlet) {
        this.servlet = servlet;
    }

    public String getServletName() {
        return servletName;
    }

    public void setServletName(String servletName) {
        this.servletName = servletName;
    }

    public String getServletClass() {
        return servletClass;
    }

    public void setServletClass(String servletClass) {
        this.servletClass = servletClass;
    }

    public String getUrlPattern() {
        return urlPattern;
    }

    public void setUrlPattern(String urlPattern) {
        this.urlPattern = urlPattern;
    }
}
