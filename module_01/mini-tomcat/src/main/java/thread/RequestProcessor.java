package thread;

import bean.Request;
import bean.Response;
import servlet.HttpServlet;

import java.net.Socket;
import java.util.Map;

/**
 * description: none
 *
 * @author thank
 * 2021/5/16 13:39
 */
public class RequestProcessor extends Thread {

    private Socket socket;

    private Map<String, HttpServlet> urlToServletMap;

    public RequestProcessor(Socket socket, Map<String, HttpServlet> urlToServletMap) {
        this.socket = socket;
        this.urlToServletMap = urlToServletMap;
    }

    @Override
    public void run() {
       try {
           Request request = new Request(socket.getInputStream());
           Response response = new Response(socket.getOutputStream());

           if (urlToServletMap.containsKey(request.getUrl())) {
               // 动态servlet处理
               HttpServlet httpServlet = urlToServletMap.get(request.getUrl());
               httpServlet.service(request, response);
           } else {
               // 静态资源处理
               response.outputHtml(null, request.getUrl());
           }

           socket.close();
       } catch (Exception e) {
           e.printStackTrace();
       }
    }
}
