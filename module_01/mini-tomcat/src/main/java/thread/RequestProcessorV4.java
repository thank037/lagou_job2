package thread;

import bean.Request;
import bean.Response;
import com.google.common.base.Splitter;
import container.Context;
import container.Host;
import container.Wrapper;
import servlet.HttpServlet;
import utils.HttpProtocolUtil;

import java.net.Socket;
import java.util.List;
import java.util.Map;

/**
 * description: none
 *
 * @author thank
 * 2021/5/16 13:39
 */
public class RequestProcessorV4 extends Thread {


    private Socket socket;
    private Host host;

    public RequestProcessorV4(Socket socket, Host host) {
        this.socket = socket;
        this.host = host;
    }

    @Override
    public void run() {
       try {
           Request request = new Request(socket.getInputStream());
           Response response = new Response(socket.getOutputStream());

           // 从URL中截取: `/demo1/hello.do`  --> contextName: demo1, requestResource: /hello.do
           String url = request.getUrl();
           String contextName = Splitter.on("/").trimResults().omitEmptyStrings().split(url).iterator().next();
           Context context = getContext(host.getContexts(), contextName);

           if (context == null) {
               response.outputStr(HttpProtocolUtil.getHttp404());
               return;
           }

           Map<String, Wrapper> wrapperMap = context.getWrapperMap();
           int index = url.indexOf(contextName) + contextName.length();
           String requestResource = url.substring(index);

           if (wrapperMap.containsKey(requestResource)) {
               Wrapper wrapper = wrapperMap.get(requestResource);
               HttpServlet servlet = wrapper.getServlet();
               servlet.service(request, response);
           } else {
               response.outputHtml(host.getAppBase(), url);
           }

           socket.close();
       } catch (Exception e) {
           e.printStackTrace();
       }
    }

    private Context getContext(List<Context> contexts, String name) {
        return contexts.stream().filter(context -> context.getName().equals(name)).findFirst().orElse(null);
    }
}
