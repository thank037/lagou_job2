package bean;

import utils.HttpProtocolUtil;
import utils.StaticResourceUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * description: 封装返回对象
 * - 依赖于OutputStream
 * - 提供输出Html的方法
 *
 * @author thank
 * 2021/5/16 12:44
 */
public class Response {

    private OutputStream outputStream;

    public Response(OutputStream outputStream) {
        this.outputStream = outputStream;
    }

    /**
     * 根据url获取静态资源的绝对路径, 读取资源后通过输出流进行输出
     * @param path 其实就是请求中的url
     */
    public void outputHtml(String appBase, String path) throws IOException {
        String absolutePath = "";
        if (appBase == null || "".equals(appBase)) {
            absolutePath = StaticResourceUtil.getAbsolutePath(path);
        } else {
            absolutePath = appBase + path;
        }

        File file = new File(absolutePath);

        if (file.exists() && file.isFile()) {
            StaticResourceUtil.outputStaticResource(new FileInputStream(file), outputStream);
        } else {
            outputStr(HttpProtocolUtil.getHttp404());
        }
    }

    public void outputStr(String content) throws IOException {
        outputStream.write(content.getBytes());
    }
}
