package bean;

import java.io.IOException;
import java.io.InputStream;

/**
 * description: 根据请求信息(InputStream输入流) 封装为请求对象
 *
 * @author thank
 * 2021/5/16 12:35
 */
public class Request {

    /** 输入流, 数据从此获取 */
    private InputStream inputStream;

    /** 请求方式: GET, POST */
    private String method;

    /** e.g. /, /index.html */
    private String url;

    public Request(InputStream inputStream) throws IOException {
        this.inputStream = inputStream;

        int count = 0;
        while (count == 0) {
            count = inputStream.available();
        }
        byte[] bytes = new byte[count];
        inputStream.read(bytes);
        String inputStr = new String(bytes);
        // GET / HTTP/1.1
        String firstLine = inputStr.split("\n")[0];
        this. method = firstLine.split(" ")[0];
        this.url = firstLine.split(" ")[1];

        System.out.println("request" + method + " " + url);
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
